#AutoIt3Wrapper_AU3Check_Parameters=-d -w 1 -w 2 -w 3 -w 4 -w 5 -w 6
#include-once
#include <WinAPISys.au3>

; открыть файл, прочитать и закрыть; можно без хендлов, но так быстрее согласно документации
Func _ReadFile($sFilePath)
	Local $hFile = FileOpen($sFilePath)
	If $hFile = -1 Then Return SetError(1)
	Local $sContent = FileRead($hFile)
	If @error Then Return SetError(2)
	FileClose($hFile)
	Return $sContent
EndFunc   ;==>_ReadFile

; иногда удобно читать во внешнюю переменную - если огромные куски текста
Func _ReadFileNoRet(ByRef $sString, $sFilePath)
	Local $hFile = FileOpen($sFilePath)
	If $hFile = -1 Then Return SetError(1)
	$sString = FileRead($hFile)
	If @error Then Return SetError(2)
	FileClose($hFile)
EndFunc   ;==>_ReadFileNoRet


; проверка на занятость файла другим процессом
;
; на основе https://www.autoitscript.com/forum/topic/53994-need-help-with-copy-verification/?do=findComment&comment=410020
;
; прочее:
;   _WinAPIfileInUse работает некорректно
;	нерабочий вариант:
;	     https://www.autoitscript.com/forum/topic/148893-fileopen-detect-already-opened-file/?do=findComment&comment=1059613
;
Func _FileInUse($sFilePath)

	; _WinAPI_CreateFile отдаст 0 и при отсутствии файла,
	; и в случае занятости его другим процессом, поэтому
	; если необходимо проверять файл на существование,
	; то это необходимо делать во вне данной ф-ции

	Local Const $hFileOpen = _WinAPI_CreateFile($sFilePath, 2, 4)
	If $hFileOpen = 0 Then Return True
	_WinAPI_CloseHandle($hFileOpen)
	Return False
EndFunc   ;==>_FileInUse


; для проверки папки на запись (также можно использовать для проверки на её существование)
Func _DirWritable($sDirPath)

	Local $sFilePath
	Do
		$sFilePath = $sDirPath & '\' & Random(10000, 10000000000, 1) & ".txt"
	Until Not FileExists($sFilePath)

	Local $hFile = FileOpen($sFilePath, 1)
	If $hFile = -1 Then
		Return False
	Else
		FileClose($hFile)
		FileDelete($sFilePath)
		Return True
	EndIf
EndFunc   ;==>_DirWritable
