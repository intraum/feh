#AutoIt3Wrapper_AU3Check_Parameters=-d -w 1 -w 2 -w 3 -w 4 -w 5 -w 6
#include-once
#include <Array.au3>
#include <File.au3>


#Region получение файлов по их атрибутам времени -------

; на основе https://www.autoitscript.com/forum/topic/19129-finding-the-latest-file/?do=findComment&comment=1347403

; #FUNCTION# ====================================================================================================================
; Name ..........: _FilesTimesFromDir
; Description ...:
;
; Syntax ........: _FilesTimesFromDir($sDirPath[, $sFilesMask = "*.*"[, $iTimestampType = 0[, $bRetArray = True]]])
; Parameters ....: $sDirPath           - Folder Path to Search
;                  $sFilesMask         - [optional] File Mask. Default is "*.*".
;                  $iTimestampType     - [optional] Flag to indicate which timestamp
;                                       -     $FT_MODIFIED (0) = Last modified (default)
;                                       -     $FT_CREATED (1) = Created
;                                       -     $FT_ACCESSED (2) = Last accessed
;                  $bRetArray          - [optional] To specify type of return
;                                       -     True = Return an Array (default)
;                                       -     False = Return latest Filename for iTimestampType
; Return values .: An array or string
; Author ........: Subz
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ====================================================================================================================
Func _FilesTimesFromDir($sDirPath, $sFilesMask = "*.*", $iTimestampType = 0, $bRetArray = True)
	Local $aFilesPaths = _FileListToArrayRec($sDirPath, $sFilesMask, 1, 0, 0, 2)
	If @error Then Return 0
	Return __FilesTimesPrep($aFilesPaths, Default, $iTimestampType, $bRetArray)
EndFunc   ;==>_FilesTimesFromDir

; #FUNCTION# ===================================================================================================================
; Name ..........: _FilesTimesFromArray
; Description ...:
;
; Syntax ........: _FilesTimes($aFilesPaths[, $bHasCounter = True[, $iTimestampType = 0[, $bRetArray = True]]])
; Parameters ....: $aFilesPaths         - Array with files' paths.
;                  $bHasCounter         - [optional] Whether Array has counter in first (zero) element.
;										  Default is True.
;                  $iTimestampType      - [optional] Flag to indicate which timestamp
;                                         -     $FT_MODIFIED (0) = Last modified (default)
;                                         -     $FT_CREATED (1) = Created
;                                         -     $FT_ACCESSED (2) = Last accessed
;                  $bRetArray           - [optional] To specify type of return
;                                         -     True = Return an Array (default)
;                                         -     False = Return latest Filename for iTimestampType
; Return values .: An array or string
; Author ........: Subz and me
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===================================================================================================================
Func _FilesTimesFromArray($aFilesPaths, $bHasCounter = True, $iTimestampType = 0, $bRetArray = True)
	If Not IsArray($aFilesPaths) Then Return 0
	Return __FilesTimesPrep($aFilesPaths, $bHasCounter, $iTimestampType, $bRetArray)
EndFunc   ;==>_FilesTimesFromArray

#EndRegion получение файлов по их атрибутам времени -------


#Region получение файлов по их атрибутам времени, служебное -------

Func __FilesTimesPrep(ByRef $aFilesPaths, $bHasCounter = True, $iTimestampType = 0, $bRetArray = True)

	Local $iUbound = $bHasCounter ? $aFilesPaths[0] : UBound($aFilesPaths) - 1
	Local $iFirstElement = $bHasCounter ? 1 : 0

	Local $aFilesTimes[0][4]
	_ArrayAdd($aFilesTimes, $iUbound & "|Modified|Created|Accessed")

	For $i = $iFirstElement To $iUbound
		_ArrayAdd($aFilesTimes, $aFilesPaths[$i] & "|" & FileGetTime($aFilesPaths[$i], 0, 1) _
				 & "|" & FileGetTime($aFilesPaths[$i], 1, 1) & "|" & FileGetTime($aFilesPaths[$i], 2, 1))
	Next

	Switch $iTimestampType
		Case 1
			_ArraySort($aFilesTimes, 1, 1, 0, 2)
		Case 2
			_ArraySort($aFilesTimes, 1, 1, 0, 3)
		Case Else
			_ArraySort($aFilesTimes, 1, 1, 0, 1)
	EndSwitch

	Return $bRetArray = True ? $aFilesTimes : $aFilesTimes[1][0]
EndFunc   ;==>__FilesTimesPrep

#EndRegion получение файлов по их атрибутам времени, служебное -------
